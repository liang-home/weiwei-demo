package org.liangl.weiwei.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.liangl.common.utils.PageUtils;
import org.liangl.weiwei.coupon.entity.SeckillSkuNoticeEntity;

import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:03:49
 */
public interface SeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

