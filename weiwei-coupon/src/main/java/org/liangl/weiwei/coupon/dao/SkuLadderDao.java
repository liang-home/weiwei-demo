package org.liangl.weiwei.coupon.dao;

import org.liangl.weiwei.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:03:47
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
