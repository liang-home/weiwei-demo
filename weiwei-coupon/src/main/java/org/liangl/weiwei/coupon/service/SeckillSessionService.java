package org.liangl.weiwei.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.liangl.common.utils.PageUtils;
import org.liangl.weiwei.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:03:48
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

