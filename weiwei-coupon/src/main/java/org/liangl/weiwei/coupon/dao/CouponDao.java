package org.liangl.weiwei.coupon.dao;

import org.liangl.weiwei.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:03:49
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
