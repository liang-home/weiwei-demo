package org.liangl.weiwei.coupon.dao;

import org.liangl.weiwei.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:03:48
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
