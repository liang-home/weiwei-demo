package org.liangl.weiwei.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 *
 * @author LiangL
 * @createDate 2020/11/24 23:10
 */
@EnableDiscoveryClient
@MapperScan("org.liangl.weiwei.coupon.dao")
@SpringBootApplication
public class WeiweiCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiweiCouponApplication.class, args);
    }
}
