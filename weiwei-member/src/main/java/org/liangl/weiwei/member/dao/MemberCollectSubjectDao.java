package org.liangl.weiwei.member.dao;

import org.liangl.weiwei.member.entity.MemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:22:53
 */
@Mapper
public interface MemberCollectSubjectDao extends BaseMapper<MemberCollectSubjectEntity> {
	
}
