package org.liangl.weiwei.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author LiangL
 * @createDate 2020/11/24 23:24
 */
@EnableFeignClients(basePackages = "org.liangl.weiwei.member.feign")
@EnableDiscoveryClient
@MapperScan("org.liangl.weiwei.member.dao")
@SpringBootApplication
public class WeiweiMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiweiMemberApplication.class, args);
    }
}
