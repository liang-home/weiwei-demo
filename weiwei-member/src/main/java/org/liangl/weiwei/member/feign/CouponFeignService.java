package org.liangl.weiwei.member.feign;

import org.liangl.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author LiangL
 * @createDate 2020/11/25 22:10
 */
@FeignClient("weiwei-coupon")
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/member/list")
    public R membercoupons();
}
