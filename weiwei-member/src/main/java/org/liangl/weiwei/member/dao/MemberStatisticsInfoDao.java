package org.liangl.weiwei.member.dao;

import org.liangl.weiwei.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:22:53
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
