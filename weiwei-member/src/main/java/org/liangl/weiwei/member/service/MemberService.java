package org.liangl.weiwei.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.liangl.common.utils.PageUtils;
import org.liangl.weiwei.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:22:54
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

