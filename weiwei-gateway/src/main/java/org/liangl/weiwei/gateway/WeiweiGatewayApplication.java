package org.liangl.weiwei.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author LiangL
 * @createDate 2020/11/26 22:23
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class WeiweiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiweiGatewayApplication.class, args);
    }
}
