package org.liangl.weiwei.ware.dao;

import org.liangl.weiwei.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:48:37
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
