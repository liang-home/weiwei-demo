package org.liangl.weiwei.ware;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author LiangL
 * @createDate 2020/11/24 23:44
 */
@MapperScan("org.liangl.weiwei.ware.dao")
@SpringBootApplication
public class WeiweiWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiweiWareApplication.class, args);
    }
}
