package org.liangl.weiwei.ware.dao;

import org.liangl.weiwei.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:48:36
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
