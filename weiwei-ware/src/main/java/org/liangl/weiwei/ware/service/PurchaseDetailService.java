package org.liangl.weiwei.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.liangl.common.utils.PageUtils;
import org.liangl.weiwei.ware.entity.PurchaseDetailEntity;

import java.util.Map;

/**
 * 
 *
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:48:37
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

