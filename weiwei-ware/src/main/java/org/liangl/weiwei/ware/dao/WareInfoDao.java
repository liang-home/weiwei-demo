package org.liangl.weiwei.ware.dao;

import org.liangl.weiwei.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:48:36
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
