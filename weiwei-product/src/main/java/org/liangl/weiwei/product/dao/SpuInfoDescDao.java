package org.liangl.weiwei.product.dao;

import org.liangl.weiwei.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 22:03:42
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
