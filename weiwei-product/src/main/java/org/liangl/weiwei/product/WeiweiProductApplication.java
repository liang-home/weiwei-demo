package org.liangl.weiwei.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author LiangL
 * @createDate 2020/11/24 22:07
 */
@EnableDiscoveryClient
@MapperScan("org.liangl.weiwei.product.dao")
@SpringBootApplication
public class WeiweiProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiweiProductApplication.class, args);
    }
}
