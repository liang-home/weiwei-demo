package org.liangl.weiwei.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.liangl.common.utils.PageUtils;
import org.liangl.weiwei.product.entity.SpuImagesEntity;

import java.util.Map;

/**
 * spu图片
 *
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 22:03:42
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

