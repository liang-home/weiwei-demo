package org.liangl.weiwei.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.liangl.common.utils.PageUtils;
import org.liangl.weiwei.product.entity.SkuInfoEntity;

import java.util.Map;

/**
 * sku信息
 *
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 22:03:43
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

