package org.liangl.weiwei.product.dao;

import org.liangl.weiwei.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 22:03:43
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
