package org.liangl.weiwei.order.dao;

import org.liangl.weiwei.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:42:59
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
