package org.liangl.weiwei.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author LiangL
 * @createDate 2020/11/24 23:44
 */
@MapperScan("org.liangl.weiwei.order.dao")
@SpringBootApplication
public class WeiweiOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeiweiOrderApplication.class, args);
    }
}
