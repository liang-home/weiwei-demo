package org.liangl.weiwei.order.dao;

import org.liangl.weiwei.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author LiangL
 * @email liang_work2020@163.com
 * @date 2020-11-24 23:42:58
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}
